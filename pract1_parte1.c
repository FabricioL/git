
//Fabricio Loor
#include <stdio.h>
#include <string.h>
int main(){
	int i = 102;
	int j = -56;
	long ix = -158693157400;
	unsigned u = 35460;
	float x = 12.687;
	double dx = 0.000000025;
	char c = 'C';


	printf("%04d,%04d,%14.8f,%14.8lf \n", i, j, x, dx); //A
	printf("%04d\n%04d\n%14.8f\n%14.8lf\n", i, j, x, dx); //B
	printf("%05d, %12ld, %05d, %14.5f, %u\n", i, ix, j, x, u); //C
	printf("%05d, %12ld, %05d \n\n%14.5f, %u\n", i, ix, j, x, u); //D
	printf("%06d %6d %c\n", i, u, c); //E
	printf("%05d %05i %011.4f\n", j, u, x); //F
	printf("%-5d %-5i %-11.4f\n", j, u, x); //G
	printf("%+5d %+5i %+11.4f\n", j, u, x); //H
	printf("%05d %05i %011.4f\n", j, u, x); //I
	printf("%05d %05i %011.f\n", j, u, x); //J
	
	////Ejercicio 2

	//1

	char st[] = "Por favor, introduce tu nombre:  ";
	printf("%s",st);
	char nombre[100];
	scanf("%s",nombre);


	//2

	float x1 = 8.0, x2 = -2.5;
	printf("Ingregar valor de x1: ");
	scanf("%f",&x1);
	printf("Ingregar valor de x2: ");
	scanf("%f",&x2);

	printf("x1= %.1f,x2= %.1f\n",x1,x2);

	//3

	int a, b;
	printf("Ingregar valor de a: ");
	scanf("%d",&a);

	printf("Ingregar valor de b: ");
	scanf("%d",&b);

	printf("Suma %d\n", a+b);

}