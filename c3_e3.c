#include <stdio.h>
#include <stdlib.h>

struct list_f {
    struct list_f *l_next;
    struct list_f *l_prev;
    int l_value;
};

struct list_f* init();
void push( struct list_f *list, int value );
void pop( struct list_f *list );
int shift( struct list_f *list );
void unshift( struct list_f *list, int value );
void print( struct list_f *list );

struct list_f* init() {
    struct list_f *ret;

    ret = ( struct list_f* )malloc( sizeof( *ret ) );
    ret->l_next = ret;
    ret->l_prev = ret;
    ret->l_value = 0;

    return ret;
}

void push( struct list_f *list, int value ) {
    struct list_f *element;
    struct list_f *tail = list->l_prev;

    element = ( struct list_f* )malloc( sizeof( *element ) );
    element->l_next = list;
    element->l_prev = tail;
    element->l_value = value;
    tail->l_next = element;
    list->l_prev = element;
}

void delete( struct list_f *list, struct list_f *rem ) {
    rem->l_prev->l_next = rem->l_next;
    rem->l_next->l_prev = rem->l_prev;
}

void pop( struct list_f *list ) {
    delete( list, list->l_prev );
}

int shift( struct list_f *list ) {
    int ret = list->l_next->l_value;
    delete( list, list->l_next );

    return ret;
}

void unshift( struct list_f *list, int value ) {
    struct list_f *prep;
    prep = ( struct list_f* )malloc( sizeof( *prep ) );
    prep->l_value = value;
    prep->l_prev = list;
    prep->l_next = list->l_next;
    list->l_next = prep;
    list->l_next->l_prev = prep;
}

void print( struct list_f *list ) {
    struct list_f *l;
    for ( l = list->l_next; l != list; l = l->l_next ) {
        printf( "%d ", l->l_value );
    }
    putchar( '\n' );
}

int main() {
    int dd =0;
    char st2[] = "Elegir opción\n 1-Insertar \t 2-Borrar \n 3- Shift \t 4- Unshift\n5- print(-1 terminar):  ";
    char st3[] = "Ingrese valor (Insert):  ";
    char st4[] = "Ingrese valor (Unshift):  ";
    struct list_f * ll = init();
    printf("%s\n",st2);
    scanf("%d", &dd);
    while(dd != -1){
        switch(dd){
        case 1:
            printf("%s\n",st3);
            scanf("%d", &dd);
            push(ll, dd);
            break;
        case 2:
            pop(ll);
            break;
        case 3:
            shift(ll);
            break;
        case 4:
            printf("%s\n",st3);
            scanf("%d", &dd);
            unshift(ll, dd);
            break;
        case 5:
            print(ll);
            break;
        default:
            printf("Opcion incorrecta \n");
        }
        printf("%s\n",st2);
        scanf("%d", &dd);
    }
    return 0;
}
