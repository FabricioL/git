//Fabricio Loor
#include <stdio.h>
#include <string.h>



int valor(int n, int k){
    int res = 1;
    if (k > n - k)
    k = n - k;
    for (int i = 0; i < k; ++i){   
        res *= (n - i);
        res /= (i + 1);
    }
    return res;
}

void imprimir(int n){
    char linea [100];
    for (int co = 0; co < n; co++){
        for(int j = 0; j < n-co; j++)
            printf(" ");
        for (int i = 0; i <= co; i++){
            printf("%*d",n,valor(co, i));
        }
        printf("\n");
    }
}

int main(){
    int cantidad;
    printf("Rows: ");
    scanf("%d", &cantidad); 
    imprimir(cantidad);
    
}