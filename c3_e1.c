//Fabricio Loor 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


struct pal_con
{
	char * p;
	int c;
	struct pal_con * l_next;
};

int main(){
	char frase [] = "Hello Hello Hello Linux Ha VA Linux VA Ha Ha Ha Ha";
	//printf("Phase: ");
	//scanf("%s", frase); 

	struct pal_con * list = NULL;
	struct pal_con * acc = NULL;
	
	char * token = strtok(frase, " ");
	int flag = 1;

	struct pal_con * new;

	while( token != NULL ) {
		int fin = 0;
		do{
			if(list != NULL){
				if(strcmp(token,list->p)==0){
					list->c++;
					fin = 1;
				}else{
					list = list->l_next;
				}
			}else{
				new = (struct pal_con *)malloc(sizeof(struct pal_con));
				new->l_next = acc;
				new->p = token;
				new->c = 1;
				acc = new;
				fin = 1;
			}
		}while(fin == 0);
      	token = strtok(NULL, " ");
  		if(flag){
  			flag = 0;
  			list = acc;
  		}else
  			list = acc;
   	}

   	struct pal_con * acc2 = acc;
   	
   	while(acc2 != NULL){
   		printf("%s = %d \n",acc2->p,acc2->c);
   		acc2 = acc2 -> l_next;
   	}


}