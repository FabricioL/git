#include <stdio.h>
#include <string.h>

#define MAX_NOMBRE 50
int main(){
	char  input [5];

	char ch;
	int cant_palabras = 0;
	int en_palabra = 0;
	char file_name[MAX_NOMBRE] ;//=  "file1.txt";

	FILE *fp;

	printf("Nombre del archivo: ");
	scanf("%s", file_name);

	fp = fopen(file_name, "r");

	if(fp == NULL) {
		printf("Error %s\n", file_name);
		return 1;
	}

	while ((ch = fgetc(fp)) != EOF) {
		if(ch == ' ' || ch == '\t' || ch == '\0' || ch == '\n') {
		  if (en_palabra) {
		    en_palabra = 0;
		    cant_palabras++;
		  }
		  
		} else {
		  en_palabra = 1;
		}
	}
	printf("Cantidad de palabras: %d\n",cant_palabras);

	return 0;
}