#include <stdio.h>
#include <math.h>

#define swapa(type,a,b) type t = a;a = b;b = t;



int comp_abs_m(float a, float b){
  return fabs(a)<=fabs(b);
}

int comp_alg_m(float a, float b){
  return a <= b;
}

int comp_abs_M(float a, float b){
  return fabs(a) >fabs(b);
}

int comp_alg_M(float a, float b){
  return a > b;
}


int partition(float array[], int low, int high, int func(float,float)) {
  int pivot = array[high];
  int i = (low - 1);
  for (int j = low; j < high; j++) {
    if (func(array[j], pivot)) {
    	i++;
    	swapa(float,array[i],array[j]);
    }
  }
  swapa(float,array[i + 1],array[high]);
  return (i + 1);
}

void quickSort(float array[], int low, int high, int func(float,float)) {
  if (low < high) {
    int pi = partition(array, low, high, func);
    quickSort(array, low, pi - 1, func);
    quickSort(array, pi + 1, high, func);
  }
}

void printArray(float array[], int size_array ){
	for (int i = 0; i < size_array; ++i)
	{
		printf("%.2f ",array[i]);
	}
	printf("\n");
}


void copyArray(float array1[], float array2[] , int size_array){
	for (int i = 0; i < size_array; ++i)
	{
		array2[i] = array1[i];
	}
}


int main(){
	float ar [] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7};
	int size = sizeof(ar)/sizeof(float);

	printf("%d\n",size);
	printArray(ar, size);

	float ar2 [size];
	copyArray(ar, ar2, size);


	quickSort(ar2, 0, size-1, comp_abs_m);

	printArray(ar2, size);

	float ar3 [size];
	copyArray(ar, ar3, size);

	quickSort(ar3, 0, size-1, comp_alg_m);

	printArray(ar3, size);


	float ar4 [size];
	copyArray(ar, ar4, size);

	quickSort(ar4, 0, size-1, comp_abs_M);

	printArray(ar4, size);

	quickSort(ar4, 0, size-1, comp_alg_M);

	printArray(ar4, size);

	return 0;

}