#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h> 

#define NUM_CARTAS 48
#define NUM_CARTAS_PALOS 12
#define STOP 5

int parse_carta(int a){
	if(a == 10 || a == 11 || a == 12)
		return 10;
	if(a == 0)
		return 11;
	return a;
}


void jugar(){

	int cartas_j1 [NUM_CARTAS],cartas_j2 [NUM_CARTAS];
	int * cartas = (int *) calloc(4, NUM_CARTAS_PALOS * sizeof(int));
	int j1 = 0, j2 = 0;
	int carta ;
	int manoj1=0, manoj2=0;
	for (int i = 0; i < STOP; ++i){
	
		do{
			carta = (rand() % NUM_CARTAS_PALOS) +1;
		}while(cartas[carta]>0 && manoj1+manoj2 < NUM_CARTAS);
		cartas_j1[manoj1++] = carta;
		cartas[carta]--;
		j1+= carta;
		do{
			carta = (rand() % NUM_CARTAS_PALOS) +1;
		}while(cartas[carta]>0 && manoj1+manoj2 < NUM_CARTAS);
		cartas_j1[manoj1++] = carta;
		cartas[carta]--;
		j1+= carta;
		do{
			carta = (rand() % NUM_CARTAS_PALOS) +1;
		}while(cartas[carta]>0 && manoj1+manoj2 < NUM_CARTAS);
		cartas_j2[manoj2] = carta;
		cartas[carta]--;
		j2+= carta;
		do{
			carta = (rand() % NUM_CARTAS_PALOS) +1;
		}while(cartas[carta]>0 && manoj1+manoj2 < NUM_CARTAS);
		cartas_j2[manoj2++] = carta;	
		cartas[carta]--;
		j2+= carta;
	}
		
	if(abs(21 - j1) < abs(21 - j2)){
		printf("Gano el jugardor 1. Puntajes J1=%d J2=%d\n", abs(21 - j1), abs(21 - j2));
	}
	else 
		if(abs(21 - j2) < abs(21 - j1)){
			printf("Gano el jugardor 2. Puntajes J1=%d J2=%d\n", abs(21 - j1), abs(21 - j2));
		}
		else{ 
			printf("Empate");
		}

}


int main(){
	char  input [6];

	time_t t;	
	srand((unsigned) time(&t));
	
	printf("Bienvenido al BlackJack. Teclear jugar|ayuda|salir.\n");
	scanf("%s", input);
	printf("%s\n",input);
	while (strcmp(input, "salir") != 0){
	    if (strcmp(input, "jugar") == 0){
	    	jugar();
	    }else if (strcmp(input, "ayuda") == 0){
	        printf("Tipear 'jugar' para empezar el juego.\n");
	    }
	    printf("Teclear jugar|ayuda|salir.\n");
	    scanf("%s", input);
	}	

	return 0;
}