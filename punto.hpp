//Fabricio Loor
#include <stdio.h>
#include <string.h>
#include <cmath>

#include <iostream>

#define CANT 5


using namespace std;

class Punto
{
private:
	double x;
	double y;
public:
	
	double distancia_dos(Punto b){
		double dx = b.x - x;
    	double dy = b.y - y;
		return sqrt(dx * dx + dy * dy);
	}
	double distancia_origen(){
		return sqrt(x * x + y * y);
	}
	void setPunto(double p, double q){
		x= p;
		y=q;
	}
	double getX(){
		return x;
	}
	double getY(){
		return y;
	}
};