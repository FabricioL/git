#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>


struct equipo
{
	char * equipo;
	uint32_t vict;
	uint32_t derr;
	uint32_t emp;
	uint32_t gol;
	struct equipo * e_next;
};

int comp_vict(struct equipo a, struct equipo b){
  return a.vict>b.vict;
}

int comp_derr(struct equipo a, struct equipo b){
  return a.derr>b.derr;
}

int comp_gol(struct equipo a, struct equipo b){
  return a.gol>b.gol;
}


void printList(struct equipo *acc)
{
    struct equipo *temp = acc;
    while (temp!=NULL)
    {
        printf("Equipo %s V%d D%d E%d G%d \n", temp->equipo,temp->vict,temp->derr,temp->emp,temp->gol);
        temp = temp->e_next;
    }
}
  
void swap(struct equipo *a, struct equipo *b)
{
    struct equipo * temp = a;
    a = b;
    b = temp;
}


void bubbleSort(struct equipo *acc, int func(struct equipo ,struct equipo ))
{
    int swapped, i;
    struct equipo *ptr1;
    struct equipo *lptr = NULL;
  
    if (acc == NULL)
        return;
  
    do
    {
        swapped = 0;
        ptr1 = acc;
        while (ptr1->e_next != lptr)
        {
            if (func(*ptr1,*ptr1->e_next))
            { 
                swap(ptr1, ptr1->e_next);
                swapped = 1;
            }
            ptr1 = ptr1->e_next;
        }
        lptr = ptr1;
    }
    while (swapped);
}

struct equipo  *  agregar( struct equipo  *list ) {
    struct equipo  *nuevo = ( struct equipo * )malloc( sizeof( *nuevo ) );
    nuevo->equipo = (char * )malloc( sizeof( 10 ) );
    nuevo->e_next = list;

    printf("Nombre equipo: ");
    scanf("%s", nuevo->equipo);
    printf("Victorias: ");
    scanf("%d", &nuevo->vict);
    printf("Derrotas: ");
    scanf("%d", &nuevo->derr);
    printf("Empates: ");
    scanf("%d", &nuevo->emp);
    printf("Goles: ");
    scanf("%d", &nuevo->gol);

    return nuevo;
    
}


int main(){
	int dd;
	printf("Bienvenido agregar equipos.\n");
	char st2[] = "Elegir opción\n 1-Agregar equipo \t 2-Ordenar x Victorias \n 3- Ordenar x Derrotas \t 4- Ordenar x goles \n 5- Imprimir\t-1 SALIR\n  ";
    
    struct equipo * acceso = NULL;
    printf("%s\n",st2);
    scanf("%d", &dd);
    struct equipo * acceso1;
    while(dd != -1){
        switch(dd){
        case 1:
            acceso = agregar(acceso);
            break;
        case 2:
        	acceso1 = acceso;
            bubbleSort(acceso1, comp_vict);
            printList(acceso);
            break;
        case 3:
        	acceso1 = acceso;
            bubbleSort(acceso1, comp_derr);
            printList(acceso);
            break;
        case 4:
        	acceso1= acceso;
            bubbleSort(acceso1, comp_gol);
            printList(acceso);
            break;
        case 5:
        	printList(acceso);
            break;
        default:
            printf("Opcion incorrecta \n");
        }
        printf("%s\n",st2);
        scanf("%d", &dd);
    }
    return 0;
}